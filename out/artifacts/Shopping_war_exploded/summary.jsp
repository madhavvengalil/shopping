<%@ page import="com.madhav.domain.Item" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 6/28/2020
  Time: 6:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form method="post" action="${pageContext.request.contextPath}/cs">
<h1>Items</h1>
    <p style="margin-left: 500px;" ><a href="help.jsp">Help</a>/<a  href="${pageContext.request.contextPath}/logout.jsp">Logout</a></p>
<table>
    <tr>
        <th>Name</th>
        <th>Quantity</th>
        <th>Price</th>
    </tr>
<%
   Map<Integer, Item> cart = (Map<Integer, Item>) session.getAttribute("cartItems");
    Map<Integer, Float> price = (Map<Integer, Float>) session.getAttribute("price");
    for(Map.Entry<Integer, Float> item : price.entrySet())
    {
%>
<tr>
    <td><%=cart.get(item.getKey()).getName()%></td>
    <td><%= item.getValue()/cart.get(item.getKey()).getPrice()%></td>
    <td><%=item.getValue() %></td>
</tr>
    <%
        }
    %>

</table>
    <hr>

    <p>GrandTotal : <%=session.getAttribute("grandTotal")%></p>
    <input type="submit" name="action" value="Back To cart"/>
    <input type="submit" name="action" value="checkout"/>
    <input type="hidden" name="page" value="summary"/>
</form>

</body>
</html>
