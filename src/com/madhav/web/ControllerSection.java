package com.madhav.web;

import com.madhav.domain.Item;
import com.madhav.service.CredentialService;
import com.madhav.service.ItemService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ControllerSection extends HttpServlet {
    private CredentialService credentialService;
    private ItemService itemService;
    private HttpSession session = null;
    @Override
    public void init(ServletConfig config) throws ServletException {
        //
        credentialService = new CredentialService();
        itemService = new ItemService();

    }

    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException {
        String nextPage = "/login.jsp";
        String page = request.getParameter("page");
        String action = request.getParameter("action");

        if(page != null && page.equalsIgnoreCase("login") && action.equalsIgnoreCase("login"))
        {

            String username = request.getParameter("username");
            String password = request.getParameter("password");


            boolean isValid = credentialService.authenticateUser(username,password);
            if(isValid)
            {
                nextPage = "/items.jsp";
                request.setAttribute("itemsList",itemService.all());
                session = request.getSession(true);
            }
            else {
                nextPage = "/login.jsp";
                request.setAttribute("loginError", "Invalid username or password");
            }
        }
        else if(page != null && page.equalsIgnoreCase("items") && action.equalsIgnoreCase("Add to Cart")){

            String[] id = request.getParameterValues("itemsChecked");
            List<Item> itemList = itemService.all();
            Map<Integer,Item> cartItems = new TreeMap<>();
            Map<Integer,Float> totalPrice = new TreeMap<>();
            for(int i = 0; i < id.length;i++ ) {

                if (itemList.get(Integer.parseInt(id[i])-1).getId() == Integer.parseInt(id[i])) {

                    cartItems.put(Integer.parseInt(id[i]), itemList.get(Integer.parseInt(id[i])-1));
                    totalPrice.put(Integer.parseInt(id[i]),Float.parseFloat(request.getParameter(id[i]))*itemList.get(Integer.parseInt(id[i])-1).getPrice());

                }
            }
            float sum = (float) totalPrice.values().stream().mapToDouble(Float::doubleValue).sum();
            nextPage = "/items.jsp";
            session.setAttribute("cartItems",cartItems);
            session.setAttribute("price",totalPrice);
            session.setAttribute("grandTotal",sum);
            request.setAttribute("itemsList",itemService.all());
            request.setAttribute("addToCart","Items added to cart Successfully");

        }
        else if(page != null && page.equalsIgnoreCase("items") && action.equalsIgnoreCase("checkout"))
        {
            nextPage = "/summary.jsp";

        }
        else if(page != null && page.equalsIgnoreCase("summary") && action.equalsIgnoreCase("Back To Cart"))
        {
            nextPage = "/items.jsp";
            request.setAttribute("itemsList",itemService.all());

        }
        else if(page != null && page.equalsIgnoreCase("summary") && action.equalsIgnoreCase("checkout")){
            nextPage = "/thankyou.jsp";
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }
}
