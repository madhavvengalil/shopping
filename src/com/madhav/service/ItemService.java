package com.madhav.service;

import com.madhav.domain.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemService {
    public List<Item> all()
    {
        List<Item> items = new ArrayList<>();
        items.add(new Item(1,"Lamp",24.4f));
        items.add(new Item(2,"Mango",45.6f));
        items.add(new Item(3,"Lamp1",24.4f));
        items.add(new Item(4,"Mango2",45.6f));
        items.add(new Item(5,"Lamp3",24.4f));
        items.add(new Item(6,"Mango4",45.6f));

        return items;
    }
}
