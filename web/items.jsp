<%@ page import="com.madhav.domain.Item" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 6/28/2020
  Time: 12:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of Items</title>

</head>
<body>
<%
    String addToCart = (String)request.getAttribute("addToCart");
    //System.out.println(addToCart);
    if(addToCart != null) {
%>
<span style="color: #4cae4c"><%=addToCart%></span>
<%
    }
%>
<p style="margin-left: 500px;" ><a href="help.jsp">Help</a>/<a  href="${pageContext.request.contextPath}/logout.jsp">Logout</a></p>
<form method="post" action="${pageContext.request.contextPath}/cs">

    <h2>Items</h2>
    <table>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
        </tr>
        <%
            List<Item> itemList = (List<Item>) request.getAttribute("itemsList");

            for(Item item : itemList)
            {
        %>
        <tr>
            <td><input type="checkbox" name="itemsChecked" value="<%=item.getId()%>"/></td>
            <td><%=item.getName()%></td>
            <td><%=item.getPrice()%></td>
            <td><input type="text" name="<%=item.getId()%>" /></td>

        </tr>
        <%
            }
        %>

    </table>
    <input type="submit" name="action"  value = "Add to Cart"/>
    <input type="submit" name="action" value="checkout"/>
    <input type="hidden" name="page" value="items"/>
</form>
</body>
</html>
