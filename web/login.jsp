<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 6/24/2020
  Time: 7:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <style type="text/css">
        <%@include file="bootstrap/css/bootstrap.css" %>
        <%@include file="bootstrap/css/bootstrap-theme.css" %>
        <%@include file="stylesheets/loginStyles.css" %>
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>


</head>
<body>
<%
    String loginError = (String)request.getAttribute("loginError");
    if(loginError != null) {
%>
<div class="container">
    <div class="row invalidUser">
        <span><%=loginError %></span>
    </div>
</div>

<%
    }
%>
<%
    String logout = (String)request.getAttribute("logoutSuccessfull");
    System.out.println(logout);
    if(logout != null) {
%>
<span style="color: #2b542c"><%=logout%></span>
<%
    }
%>
<div class="container loginForm">

    <h1 style="align-content: center">Login Form</h1>
    <form method="post" action="${pageContext.request.contextPath}/cs">
        <div class="form-group">
            <label>Username:</label>
            <input type="text" class="form-control" name="username" placeholder="username"/>
        </div>
        <div class="form-group">
            <label>Password:</label>
            <input type="password" class="form-control" name="password" placeholder="password"/>
        </div>

        <input type="submit" name="action" value="login" class="btn btn-primary"/>
        <input type="reset" name="reset" class="btn btn-info" style="margin-left:40px;"/>
        <input type="hidden" name="page" value="login"/>
    </form>

</div>


</body>
</html>
